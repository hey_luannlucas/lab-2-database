package Tabelas;

import java.sql.*;

/**
 * Classe responsável por gerenciar as operações relacionadas aos alunos no banco de dados.
 */

public class AlunoManager {
    private Connection connection;

    /**
     * Construtor da classe AlunoManager.
     * @param connection A conexão com o banco de dados.
     */

    public AlunoManager(Connection connection) {
        this.connection = connection;
    }

    /**
     * Método para inserir um novo aluno no banco de dados.
     * @param nome O nome do aluno.
     * @param idade A idade do aluno.
     * @param endereco O endereço do aluno.
     */

    public void insertAluno(String nome, int idade, String endereco) throws SQLException {
        try {
            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Alunos (nome, idade, endereco) VALUES (?, ?, ?)")) {
                statement.setString(1, nome);
                statement.setInt(2, idade);
                statement.setString(3, endereco);
                statement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new SQLException("Error inserting aluno", e);
            }
        } finally {
            connection.setAutoCommit(true);
        }
    }


    /**
     * Método para atualizar os dados de um aluno no banco de dados.
     * @param idAluno O ID do aluno.
     * @param nome O novo nome do aluno.
     * @param idade A nova idade do aluno.
     * @param endereco O novo endereço do aluno.
     */

    public void updateAluno(int idAluno, String nome, int idade, String endereco) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE Alunos SET nome = ?, idade = ?, endereco = ? WHERE id_aluno = ?")) {
            statement.setString(1, nome);
            statement.setInt(2, idade);
            statement.setString(3, endereco);
            statement.setInt(4, idAluno);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw new SQLException("Error updating aluno", e);
        }
    }


    /**
     * Método para buscar um aluno no banco de dados pelo ID.
     * @param idAluno O ID do aluno.
     */

    public void getAlunoByID(int idAluno) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM Alunos WHERE id_aluno = ?")) {
            statement.setInt(1, idAluno);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nome = resultSet.getString("nome");
                int idade = resultSet.getInt("idade");
                String endereco = resultSet.getString("endereco");
                System.out.println("ID: " + idAluno + ", Nome: " + nome + ", Idade: " + idade + ", Endereço: " + endereco);
            } else {
                System.out.println("Aluno não encontrado com o ID: " + idAluno);
            }
        } catch (SQLException e) {
            throw new SQLException("Error getting aluno by ID", e);
        }
    }

    /**
     * Método para buscar as notas de um aluno no banco de dados pelo ID.
     * @param idAluno O ID do aluno.
     */
    public void getAlunoGradesById(int idAluno) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT Alunos.nome, Notas.nota, Materias.nome AS materia FROM Alunos " +
                        "INNER JOIN Notas ON Alunos.id_aluno = Notas.id_aluno " +
                        "INNER JOIN Materias ON Notas.id_materia = Materias.id_materia " +
                        "WHERE Alunos.id_aluno = ?")) {
            statement.setInt(1, idAluno);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String nome = resultSet.getString("nome");
                double nota = resultSet.getDouble("nota");
                String materia = resultSet.getString("materia");
                System.out.println("Nome: " + nome + ", Nota: " + nota + ", Materia: " + materia);
            }
        } catch (SQLException e) {
            throw new SQLException("Error getting aluno grades by ID", e);
        }
    }

    /**
     * Método para deletar um aluno do banco de dados pelo ID.
     * @param idAluno O ID do aluno.
     */
    public void deleteAluno(int idAluno) throws SQLException {
        try {
            connection.setAutoCommit(false);

            try (PreparedStatement deleteNotasStatement = connection.prepareStatement(
                    "DELETE FROM Notas WHERE id_aluno = ?")) {
                deleteNotasStatement.setInt(1, idAluno);
                deleteNotasStatement.executeUpdate();
            }

            try (PreparedStatement deleteAlunoStatement = connection.prepareStatement(
                    "DELETE FROM Alunos WHERE id_aluno = ?")) {
                deleteAlunoStatement.setInt(1, idAluno);
                deleteAlunoStatement.executeUpdate();
            }

            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw new SQLException("Error deleting aluno", e);
        } finally {
            connection.setAutoCommit(true);
        }
    }

    /**
     * Método para buscar todos os alunos no banco de dados.
     */
    public void getAlunos() throws SQLException {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM Alunos")) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id_aluno");
                String nome = resultSet.getString("nome");
                int idade = resultSet.getInt("idade");
                String endereco = resultSet.getString("endereco");
                System.out.println("ID: " + id + ", Nome: " + nome + ", Idade: " + idade + ", Endereço: " + endereco);
            }
        } catch (SQLException e) {
            throw new SQLException("Error getting alunos", e);
        }
    }
}