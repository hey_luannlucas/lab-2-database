package Tabelas;

import java.sql.*;

/**
 * Classe responsável por gerenciar as operações relacionadas às matérias no banco de dados.
 */
public class MateriaManager {
    private Connection connection;
    /**
     * Construtor da classe MateriaManager.
     * @param connection A conexão com o banco de dados.
     */
    public MateriaManager(Connection connection) {
        this.connection = connection;
    }
    /**
     * Método para inserir uma nova matéria no banco de dados.
     * @param nome O nome da matéria.
     */
    public void insertMateria(String nome) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Materias (nome) VALUES (?)")) {
            statement.setString(1, nome);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para buscar todas as matérias no banco de dados.
     */
    public void getMaterias() {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM Materias")) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id_materia");
                String nome = resultSet.getString("nome");
                System.out.println("ID: " + id + ", Nome: " + nome);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
