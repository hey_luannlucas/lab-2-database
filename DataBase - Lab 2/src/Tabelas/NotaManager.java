package Tabelas;

import java.sql.*;

/**
 * Classe responsável por gerenciar as operações relacionadas às notas no banco de dados.
 */
public class NotaManager {
    private Connection connection;
    /**
     * Construtor da classe NotaManager.
     * @param connection A conexão com o banco de dados.
     */
    public NotaManager(Connection connection) {
        this.connection = connection;
    }
    /**
     * Método para inserir uma nova nota no banco de dados.
     * @param idAluno O ID do aluno.
     * @param idProfessor O ID do professor.
     * @param idMateria O ID da matéria.
     * @param nota A nota do aluno na matéria.
     */
    public void insertNota(int idAluno, int idProfessor, int idMateria, double nota) throws SQLException {
        String insertNotaQuery = "INSERT INTO Notas (id_aluno, id_professor, id_materia, nota) VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(insertNotaQuery)) {
            statement.setInt(1, idAluno);
            statement.setInt(2, idProfessor);
            statement.setInt(3, idMateria);
            statement.setDouble(4, nota);
            statement.executeUpdate();

            System.out.println("Nota inserida com sucesso.");
        } catch (SQLException e) {
            throw new SQLException("Error inserting nota", e);
        }
    }

    /**
     * Método para atualizar uma nota no banco de dados.
     * @param idNota O ID da nota.
     * @param novaNota A nova nota.
     */
    public void updateNota(int idNota, double novaNota) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE Notas SET nota = ? WHERE id_nota = ?")) {
            statement.setDouble(1, novaNota);
            statement.setInt(2, idNota);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw new SQLException("Error updating nota", e);
        }
    }
}
