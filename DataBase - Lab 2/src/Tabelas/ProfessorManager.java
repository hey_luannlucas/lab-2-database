package Tabelas;

import java.sql.*;
/**
 * Classe responsável por gerenciar as operações relacionadas aos professores no banco de dados.
 */
public class ProfessorManager {
    private Connection connection;
    /**
     * Construtor da classe ProfessorManager.
     * @param connection A conexão com o banco de dados.
     */
    public ProfessorManager(Connection connection) {
        this.connection = connection;
    }

    /**
     * Método para inserir um novo professor no banco de dados.
     * @param nome O nome do professor.
     * @param idMateria O ID da matéria que o professor leciona.
     */
    public void insertProfessor(String nome, int idMateria) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Professores (nome, id_materia) VALUES (?, ?)")) {
            statement.setString(1, nome);
            statement.setInt(2, idMateria);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para atualizar os dados de um professor no banco de dados.
     * @param idProfessor O ID do professor.
     * @param nome O novo nome do professor.
     * @param disciplina A nova disciplina que o professor leciona.
     */
    public void updateProfessor(int idProfessor, String nome, String disciplina) {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE Professores SET nome = ?, id_materia = ? WHERE id_professor = ?")) {
            statement.setString(1, nome);
            statement.setInt(2, Integer.parseInt(disciplina));
            statement.setInt(3, idProfessor);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Método para deletar um professor do banco de dados pelo ID.
     * @param idProfessor O ID do professor.
     */
    public void deleteProfessor(int idProfessor) {
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM Professores WHERE id_professor = ?")) {
            statement.setInt(1, idProfessor);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Método para buscar um professor no banco de dados pelo ID.
     * @param idProfessor O ID do professor.
     */
    public void getProfessorById(int idProfessor) {
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM Professores WHERE id_professor = ?")) {
            statement.setInt(1, idProfessor);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nome = resultSet.getString("nome");
                String disciplina = resultSet.getString("disciplina");
                System.out.println("ID: " + idProfessor + ", Nome: " + nome + ", Disciplina: " + disciplina);
            } else {
                System.out.println("Professor não encontrado com o ID: " + idProfessor);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método para buscar todos os professores no banco de dados.
     */
    public void getProfessores() {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM Professores")) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id_professor");
                String nome = resultSet.getString("nome");
                String disciplina = resultSet.getString("disciplina");
                System.out.println("ID: " + id + ", Nome: " + nome + ", Disciplina: " + disciplina);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}