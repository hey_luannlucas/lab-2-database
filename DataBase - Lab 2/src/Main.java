import Gerenciadores.DatabaseSetup;
import Visualizadores.Menu;

import java.sql.Connection;

public class Main {
    public static void main(String[] args) {
        Connection connection = DatabaseSetup.setupDatabase();

        if (connection != null) {
            Menu menu = new Menu(connection);
            menu.run();
        }
    }
}