package Gerenciadores;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import Tabelas.*;

/**
 * Classe responsável por gerenciar as operações do banco de dados.
 */
class DatabaseManager {
    private Connection connection;

    /**
     * Construtor da classe DatabaseManager.
     * @param connection A conexão com o banco de dados.
     */
    public DatabaseManager(Connection connection) {
        this.connection = connection;
    }

    /**
     * Método para criar as tabelas no banco de dados.
     */
    public void createTables() {

        try (Statement statement = connection.createStatement()) {
            String createMateriasTableQuery = "CREATE TABLE IF NOT EXISTS Materias (" +
                    "id_materia INT AUTO_INCREMENT PRIMARY KEY," +
                    "nome VARCHAR(100) NOT NULL" +
                    ")";
            statement.executeUpdate(createMateriasTableQuery);

            String createProfessoresTableQuery = "CREATE TABLE IF NOT EXISTS Professores (" +
                    "id_professor INT AUTO_INCREMENT PRIMARY KEY," +
                    "nome VARCHAR(100) NOT NULL," +
                    "id_materia INT," +
                    "FOREIGN KEY (id_materia) REFERENCES Materias(id_materia)" +
                    ")";
            statement.executeUpdate(createProfessoresTableQuery);

            String createAlunosTableQuery = "CREATE TABLE IF NOT EXISTS Alunos (" +
                    "id_aluno INT AUTO_INCREMENT PRIMARY KEY," +
                    "nome VARCHAR(100) NOT NULL," +
                    "idade INT," +
                    "endereco VARCHAR(200)," +
                    "id_professor INT," +
                    "FOREIGN KEY (id_professor) REFERENCES Professores(id_professor)" +
                    ")";
            statement.executeUpdate(createAlunosTableQuery);

            String createNotasTableQuery = "CREATE TABLE IF NOT EXISTS Notas (" +
                    "id_nota INT AUTO_INCREMENT PRIMARY KEY," +
                    "id_aluno INT," +
                    "id_professor INT," +
                    "id_materia INT," +
                    "nota DECIMAL(4,2) NOT NULL," +
                    "FOREIGN KEY (id_aluno) REFERENCES Alunos(id_aluno) ON DELETE CASCADE," +
                    "FOREIGN KEY (id_professor) REFERENCES Professores(id_professor)," +
                    "FOREIGN KEY (id_materia) REFERENCES Materias(id_materia)" +
                    ")";
            statement.executeUpdate(createNotasTableQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}