package Gerenciadores;

import java.sql.*;


public class DatabaseSetup {

    /**
     * Método para configurar a conexão com o banco de dados e criar as tabelas necessárias.
     * @return A conexão com o banco de dados.
     */
    public static Connection setupDatabase() {


        String jdbc = "jdbc:mysql://127.0.0.1:3306/jala?user=root";
        String username = "root";
        String password = "12345678";


        try {
            Connection connection = DriverManager.getConnection(jdbc, username, password);

            DatabaseManager databaseManager = new DatabaseManager(connection);
            databaseManager.createTables();

            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}