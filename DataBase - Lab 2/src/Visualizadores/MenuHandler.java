package Visualizadores;

import Tabelas.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class MenuHandler {
    private Connection connection;
    private AlunoManager alunoManager;
    private ProfessorManager professorManager;
    private NotaManager notaManager;
    private MateriaManager materiaManager;

    public MenuHandler(Connection connection) {
        this.connection = connection;
        alunoManager = new AlunoManager(connection);
        materiaManager = new MateriaManager(connection);
        professorManager = new ProfessorManager(connection);
        notaManager = new NotaManager(connection);
    }

    public void handleMainMenu(Scanner scanner, String menuName, String[] options, Runnable[] actions) {
        int option;

        do {
            System.out.println("Menu " + menuName + ":");
            for (int i = 0; i < options.length; i++) {
                System.out.println((i + 1) + ". " + options[i]);
            }
            System.out.println("0. Voltar");
            System.out.print("Digite sua escolha: ");
            option = scanner.nextInt();

            if (option >= 1 && option <= options.length) {
                actions[option - 1].run();
            } else if (option != 0) {
                System.out.println("Opção inválida. Por favor, tente novamente.");
            }
        } while (option != 0);
    }

    public void handleMateriaMenu(Scanner scanner) {
        String[] options = {"Inserir Materia", "Listar Materias"};
        Runnable[] actions = {
                () -> {
                    System.out.print("Digite o nome da Materia: ");
                    String nome = scanner.next();
                    materiaManager.insertMateria(nome);
                    System.out.println("Materia inserida com sucesso.");
                },
                () -> materiaManager.getMaterias()
        };

        handleMainMenu(scanner, "Materia", options, actions);
    }

    public void handleAlunoMenu(Scanner scanner) {
        String[] options = {
                "Inserir Aluno", "Atualizar Aluno", "Deletar Aluno",
                "Buscar Alunos", "Buscar Aluno por ID", "Buscar Aluno e suas Notas por ID"
        };
        Runnable[] actions = {
                () -> {
                    System.out.print("Digite o nome do Aluno: ");
                    String nome = scanner.next();
                    System.out.print("Digite a idade do Aluno: ");
                    int idade = scanner.nextInt();
                    System.out.print("Digite o endereço do Aluno: ");
                    String endereco = scanner.next();
                    try {
                        alunoManager.insertAluno(nome, idade, endereco);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Aluno inserido com sucesso.");
                },
                () -> {
                    System.out.print("Digite o ID do Aluno: ");
                    int idAluno = scanner.nextInt();
                    System.out.print("Digite o novo nome do Aluno: ");
                    String novoNome = scanner.next();
                    System.out.print("Digite a nova idade do Aluno: ");
                    int novaIdade = scanner.nextInt();
                    System.out.print("Digite o novo endereço do Aluno: ");
                    String novoEndereco = scanner.next();
                    try {
                        alunoManager.updateAluno(idAluno, novoNome, novaIdade, novoEndereco);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Aluno atualizado com sucesso.");
                },
                () -> {
                    System.out.print("Digite o ID do Aluno: ");
                    int idAlunoToDelete = scanner.nextInt();
                    try {
                        alunoManager.deleteAluno(idAlunoToDelete);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Aluno deletado com sucesso.");
                },
                () -> {
                    try {
                        alunoManager.getAlunos();
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                },
                () -> {
                    System.out.print("Digite o ID do Aluno: ");
                    int idAlunoToPrint = scanner.nextInt();
                    try {
                        alunoManager.getAlunoByID(idAlunoToPrint);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                },
                () -> {
                    System.out.print("Digite o ID do Aluno: ");
                    int idAlunoToPrintNota = scanner.nextInt();
                    try {
                        alunoManager.getAlunoGradesById(idAlunoToPrintNota);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                }
        };

        handleMainMenu(scanner, "Aluno", options, actions);
    }

    public void handleProfessorMenu(Scanner scanner) {
        String[] options = {
                "Inserir Professor", "Atualizar Professor", "Deletar Professor",
                "Buscar Professores", "Buscar Professor por ID"
        };
        Runnable[] actions = {
                () -> {
                    System.out.print("Digite o nome do Professor: ");
                    String nome = scanner.next();
                    System.out.print("Digite o ID da Matéria do Professor: ");
                    int idMateria = scanner.nextInt();
                    professorManager.insertProfessor(nome, idMateria);
                    System.out.println("Professor inserido com sucesso.");
                },
                () -> {
                    System.out.print("Digite o ID do Professor: ");
                    int idProfessor = scanner.nextInt();
                    System.out.print("Digite o novo nome do Professor: ");
                    String novoNome = scanner.next();
                    System.out.print("Digite a nova disciplina do Professor: ");
                    String novaDisciplina = scanner.next();
                    professorManager.updateProfessor(idProfessor, novoNome, novaDisciplina);
                    System.out.println("Professor atualizado com sucesso.");
                },
                () -> {
                    System.out.print("Digite o ID do Professor: ");
                    int idProfessorToDelete = scanner.nextInt();
                    professorManager.deleteProfessor(idProfessorToDelete);
                    System.out.println("Professor deletado com sucesso.");
                },
                () -> professorManager.getProfessores(),
                () -> {
                    System.out.print("Digite o ID do Professor: ");
                    int idProfessorToPrint = scanner.nextInt();
                    professorManager.getProfessorById(idProfessorToPrint);
                }
        };

        handleMainMenu(scanner, "Professor", options, actions);
    }

    public void handleNotaMenu(Scanner scanner) {
        String[] options = {"Inserir Nota", "Atualizar Nota"};
        Runnable[] actions = {
                () -> {
                    try {
                        System.out.print("Digite o ID do Aluno: ");
                        int idAluno = scanner.nextInt();
                        System.out.print("Digite o ID do Professor: ");
                        int idProfessor = scanner.nextInt();
                        System.out.print("Digite o ID da Matéria: ");
                        int idMateria = scanner.nextInt();
                        System.out.print("Digite a Nota: ");
                        double nota = scanner.nextDouble();
                        notaManager.insertNota(idAluno, idProfessor, idMateria, nota);
                        System.out.println("Nota inserida com sucesso.");
                    } catch (SQLException e) {
                    }
                },
                () -> {
                    try {
                        System.out.print("Digite o ID da Nota: ");
                        int idNota = scanner.nextInt();
                        System.out.print("Digite a nova Nota: ");
                        double novaNota = scanner.nextDouble();
                        notaManager.updateNota(idNota, novaNota);
                        System.out.println("Nota atualizada com sucesso.");
                    } catch (SQLException e) {
                        System.out.println("Erro ao atualizar nota: " + e.getMessage());
                    }
                }
        };

        handleMainMenu(scanner, "Nota", options, actions);
    }
}