package Visualizadores;

import java.sql.*;
import java.util.Scanner;

public class Menu {
    private MenuHandler menuHandler;
    private Connection connection;

    public Menu(Connection connection) {
        this.connection = connection;
        this.menuHandler = new MenuHandler(connection);
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        int option;

        do {
            System.out.println("======Menu=======");
            System.out.println("1. Aluno");
            System.out.println("2. Professor");
            System.out.println("3. Nota");
            System.out.println("4. Materia");
            System.out.println("0. Sair");
            System.out.println();
            System.out.print("Digite sua escolha: ");
            option = scanner.nextInt();

            switch (option) {
                case 1:
                    menuHandler.handleAlunoMenu(scanner);
                    break;
                case 2:
                    menuHandler.handleProfessorMenu(scanner);
                    break;
                case 3:
                    menuHandler.handleNotaMenu(scanner);
                    break;
                case 4:
                    menuHandler.handleMateriaMenu(scanner);
                    break;
                case 0:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida. Por favor, tente novamente.");
            }
        } while (option != 0);

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}