# Projeto de Sistema de Gerenciamento Escolar

Este projeto é um sistema de gerenciamento escolar desenvolvido em Java. Ele permite ao usuário interagir com um banco de dados de uma escola, realizando operações como inserir, atualizar, deletar e buscar informações sobre alunos, professores, notas e matérias.

## Funcionamento

O sistema é baseado em um modelo de banco de dados relacional, onde temos tabelas para alunos, professores, notas e matérias. Cada uma dessas tabelas é gerenciada por uma classe específica (`AlunoManager`, `ProfessorManager`, `NotaManager` e `MateriaManager`), que contém métodos para inserir, atualizar, deletar e buscar registros.

O ponto de entrada do sistema é a classe `Menu`, que exibe um menu de opções para o usuário. Dependendo da opção escolhida, o sistema chama o método correspondente na classe `MenuHandler`, que por sua vez chama o método correspondente na classe de gerenciamento apropriada.

Por exemplo, se o usuário escolher a opção para inserir um novo aluno, o sistema chamará o método `handleAlunoMenu()` na classe `MenuHandler`, que por sua vez chamará o método `insertAluno()` na classe `AlunoManager`.

## Como Usar

Para usar o sistema, você precisa ter o Java 8 ou superior instalado em seu computador e um sistema de gerenciamento de banco de dados SQL disponível.

1. Clone ou baixe este repositório para o seu computador.
2. Abra o projeto em um IDE de sua escolha (por exemplo, IntelliJ IDEA, Eclipse).
3. Configure a conexão com o banco de dados no arquivo `DatabaseSetup.java`, substituindo `jdbc`, `username` e `password` pelos detalhes do seu banco de dados.
4. Execute o programa na classe `Main`. Na primeira execução, o sistema criará automaticamente todas as tabelas necessárias no banco de dados.
5. O sistema apresentará um menu de opções. Siga as instruções na tela para interagir com o sistema.

## Diagrama de Classes
![](Assets/diagrama.png)
## Documentação

O projeto possui documentação em JavaDoc. Para visualizá-la, abra o arquivo `index.html` localizado na pasta `doc` em um navegador web.

***

<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="Assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>


